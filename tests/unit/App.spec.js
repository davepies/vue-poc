import { shallowMount } from "@vue/test-utils";
import App from "@/App.vue";

describe("App.vue", () => {
  it("renders with a main section", () => {
    const wrapper = shallowMount(App);
    expect(wrapper.find("main").exists()).toBeTruthy();
  });
});
