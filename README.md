# Cpu-usage

## Description

![Screenshot](/screen.png)

This little project contains a tiny express app, which exposes one endpoint `GET:/api/cpu-usage`.

The endpoint provides a server-side-event stream which serves cpu-usage data of the machine
the process is running on periodically.

A frontend consumes the stream and renders a timeseries graph.

TODO:
- [ ] Improve responsive styling (heading, layout and graph)
- [ ] Test Coverage (only one scaffold test)
- [ ] Performance optimization on both the FE and BE (eg.: clean up connections, memory consumption)

## Project setup
```
yarn install
```

### Starts backend and compiles and hot-reloads for development
```
yarn run start
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
