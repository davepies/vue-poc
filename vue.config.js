module.exports = {
  devServer: {
    proxy: {
      "/api/*": {
        target: "http://localhost:3000",
        // sse events need this
        timeout: 0
      }
    }
  }
};
