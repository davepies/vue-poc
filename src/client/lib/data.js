export const createCpuUsageStream = () => {
  const es = new EventSource("/api/cpu-usage");

  // helpers
  const parsePayLoad = rawPayload => JSON.parse(rawPayload.data);
  const getBody = ({ body }) => body;

  // exports
  return {
    on(eventName, callback) {
      if (["message", "error", "close"].includes(eventName)) {
        es[`on${eventName}`] = rawPayload =>
          callback(getBody(parsePayLoad(rawPayload)));
      }
    },
    close() {
      es.close();
    }
  };
};
