const express = require("express");
const { createCpuUsageInPercentInterval } = require("./lib/osStates");
const { emit, subscribe } = require("./lib/serverSideEvents");

const app = express();

app.get("/api/cpu-usage", (req, res) => {
  subscribe(req, res);
  console.info("Client subscribed");
});

app.listen(3000, () => {
  createCpuUsageInPercentInterval(emit);
  console.info("App listening on port 3000!");
});
