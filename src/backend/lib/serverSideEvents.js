const uuid = require("uuid/v4");

const connections = [];

module.exports = {
  subscribe: function(req, res) {
    res._id = uuid();
    connections.push(res);

    res.writeHead(200, {
      "Content-Type": "text/event-stream",
      "Cache-Control": "no-cache",
      Connection: "keep-alive"
    });

    req.on("close", () => {
      // TODO: remove from connections array
      console.info("Client unsubscribed");
    });
  },
  emit: function(body) {
    const payload = { body };
    connections.forEach(res =>
      res.write(`data: ${JSON.stringify(payload)}\n\n`)
    );
    console.info(`Emitted: ${JSON.stringify(payload)}`);
  }
};
