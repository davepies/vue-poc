var os = require("os-utils");

let cpuUsageInterval = null;

module.exports = {
  createCpuUsageInPercentInterval: (callback, intervalTime = 2000) => {
    const timestampedCpuUsageInPercent = callback => {
      os.cpuUsage(usage => {
        const entry = [Date.now(), Math.floor(usage * 100)];
        callback(entry);
      });
    };

    if (!cpuUsageInterval) {
      cpuUsageInterval = setInterval(
        () => timestampedCpuUsageInPercent(callback),
        intervalTime
      );
    }

    // first emit
    timestampedCpuUsageInPercent(callback);
  }
};
